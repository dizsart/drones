## Build/Run Instructions

##### Simply open terminal in the application directory and execute the shell file. 
- #### Execute this command in the application root directory via terminal`./run_app.sh`
- #### Swagger documentation link is [here](http://localhost:8181/swagger-ui.html) 
- #### H2 database GUI location is [here](http://localhost:8181/h2)

