package com.musala.drones.serviceImpl;

import com.musala.drones.entities.Drone;
import com.musala.drones.entities.Medication;
import com.musala.drones.enums.Model;
import com.musala.drones.enums.State;
import com.musala.drones.exceptions.*;
import com.musala.drones.repository.DroneRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class DroneServiceImplTest {

    @Mock
    private  DroneRepository droneRepository;
    @InjectMocks
    private  DroneServiceImpl droneService;
    private Drone drone;
    private Drone drone2;
    private List<Medication> medications;
    private List<Drone> drones;


    @BeforeEach
    void setUp() {

        drone = Drone.builder()
                .id(1L)
                .droneState(State.IDLE)
                .model(Model.LIGHT_WEIGHT)
                .batteryPercentage(30.0/100)
                .build();

        medications = new ArrayList<>(Arrays.asList(Medication.builder()
                        .id(1L)
                        .weightInGrams(50.0)
                        .imageUrl("https://imageUrl.jpg")
                        .code("ASDFH-HSLJLS-SJHD")
                        .build(), Medication.builder()
                        .id(2L)
                        .weightInGrams(50.0)
                        .imageUrl("https://imageUrl.jpg")
                        .code("ASDFH-HSLJLS-SJHD")
                        .build(),
                Medication.builder()
                        .id(3L)
                        .weightInGrams(60.0)
                        .imageUrl("https://imageUrl2.jpg")
                        .code("ASDFH-HSLJLS-SJHD")
                        .build()));

        drone2 = Drone.builder()
                .id(2L)
                .droneState(State.IDLE)
                .model(Model.CRUISER_WEIGHT)
                .batteryPercentage(30.0/100)
                .isAvailable(true)
                .build();
        drones = List.of(drone, drone2);

    }

    @Test
    void registerDrone() {
        Mockito.when(droneRepository.save(drone)).thenReturn(drone);
        Assertions.assertEquals(drone, droneService.registerDrone(drone));
    }

    @Test
    void loadDroneMedication() throws DroneNotFoundException, MedicationLoadStatusException, DroneWeightExceededException, BatteryLowException, UnAvailableDroneException {


        drone.setMedications(medications);
        drone.setIsAvailable(true);
        Mockito.when(droneRepository.save(drone)).thenReturn(drone);
        Optional<Drone> optional = Optional.of(drone);
        Mockito.when(droneRepository.findById(drone.getId())).thenReturn(optional);

        Assertions.assertEquals(drone.getMedications(), droneService.loadDroneMedication(1L, medications).getMedications());

    }

    @Test
    void droneBatteryLevel() throws DroneNotFoundException {
        Mockito.when(droneRepository.findById(drone.getId())).thenReturn(Optional.of(drone));
        Assertions.assertEquals("30%", droneService.droneBatteryLevel( drone.getId()));
    }

    @Test
    void checkDroneLoadCapacity() {
        drone.setMedications(medications);
        double weightLimit = drone.getMedications().stream().mapToDouble(Medication::getWeightInGrams).sum();

        Assertions.assertEquals(weightLimit, droneService.checkDroneLoadCapacity(drone));
    }

    @Test
    void findAvailableDronesForLoading() {

        List<Drone> drones = new ArrayList<>(Arrays.asList(drone, drone2));
        Mockito.when(droneRepository.findAllByIsAvailableIsTrueAndBatteryPercentageIsGreaterThan(0.25)).thenReturn(drones);
        Assertions.assertEquals(2, droneService.findAvailableDronesForLoading().size());

    }

    @Test
    void findDroneById() throws DroneNotFoundException {
        Drone drone = Drone.builder()
                .id(1L)
                .droneState(State.IDLE)
                .model(Model.LIGHT_WEIGHT)
                .batteryPercentage(30.0/100)
                .isAvailable(true)
                .build();
        Mockito.when(droneRepository.findById(drone.getId())).thenReturn(Optional.of(drone));
        Assertions.assertEquals(drone, droneService.findDroneById(1L));
    }

    @Test
    void changeDroneState() throws DroneNotFoundException, BatteryLowException {
        Mockito.when(droneRepository.findById(drone.getId())).thenReturn(Optional.of(drone));
        Mockito.when(droneRepository.save(drone)).thenReturn(drone);
        drone.setDroneState(State.IDLE);
        Assertions.assertEquals(drone.getDroneState(), droneService.changeDroneState(drone.getId(), "IDLE").getDroneState());
    }


    @Test
    void findAll(){
        Mockito.when(droneRepository.findAll()).thenReturn(drones);
        Assertions.assertEquals(drones, droneService.findAll());

    }
}