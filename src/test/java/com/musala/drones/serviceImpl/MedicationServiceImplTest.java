package com.musala.drones.serviceImpl;

import com.musala.drones.entities.Medication;
import com.musala.drones.repository.MedicationRepository;
import com.musala.drones.service.MedicationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class MedicationServiceImplTest {
    @Mock
    private MedicationRepository medicationRepository;
    @InjectMocks
    private MedicationServiceImpl medicationService;
    private Medication medication;
    private List<Medication> medications;

    @BeforeEach
    void setUp() {
        medication = Medication.builder()
                .id(1L)
                .weightInGrams(50.0)
                .imageUrl("https://imageUrl.jpg")
                .code("ASDFH-HSLJLS-SJHD")
                .build();
        Medication medication2 = Medication.builder()
                .id(2L)
                .weightInGrams(50.0)
                .imageUrl("https://imageUrl.jpg")
                .code("ASDFH-HSLJLS-SJHD")
                .build();
        Medication medication1 = Medication.builder()
                .id(3L)
                .weightInGrams(60.0)
                .imageUrl("https://imageUrl2.jpg")
                .code("ASDFH-HSLJLS-SJHD")
                .build();
                medications = List.of(medication, medication1, medication2);
    }

    @Test
    void addMedication() {
        Mockito.when(medicationRepository.save(medication)).thenReturn(medication);
        Assertions.assertEquals(medication, medicationService.addMedication(medication));
    }

    @Test
    void findAllMedications() {
        Mockito.when(medicationRepository.findAll()).thenReturn(medications);
        Assertions.assertEquals(medications, medicationService.findAllMedications());
    }

    @Test
    void findListOfMedicationsByIds() {
        List<Long> medicationsId = List.of(1L, 2L, 3L);
        Mockito.when(medicationRepository.findByLoadedOnDroneFalseAndIdIn(medicationsId)).thenReturn(medications);
        Assertions.assertEquals(false, medicationService.findListOfMedicationsByIds(medicationsId).stream().findAny().get().isLoadedOnDrone());
    }
}