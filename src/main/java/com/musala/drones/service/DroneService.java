package com.musala.drones.service;

import com.musala.drones.entities.Drone;
import com.musala.drones.entities.Medication;
import com.musala.drones.exceptions.*;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

public interface DroneService {
    Drone registerDrone(Drone drone);
    Drone loadDroneMedication(Long droneId, List<Medication> medications) throws DroneNotFoundException, DroneWeightExceededException, BatteryLowException, MedicationLoadStatusException, UnAvailableDroneException;
    String droneBatteryLevel(Long droneId) throws DroneNotFoundException;
    Double checkDroneLoadCapacity(Drone droneId);
    List<Drone> findAvailableDronesForLoading();

    Drone findDroneById(Long droneId) throws DroneNotFoundException;

    Drone changeDroneState(Long droneId, String droneState) throws DroneNotFoundException, BatteryLowException;

    @Scheduled(fixedRate = 3600000) // Run every hour
    void checkBatteryLevels();

    List<Drone> findAll();
}
