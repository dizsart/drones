package com.musala.drones.service;

import com.musala.drones.entities.Medication;

import java.util.List;

public interface MedicationService {
    Medication addMedication(Medication medication);
    List<Medication> findAllMedications();

    List<Medication> findListOfMedicationsByIds(List<Long> medicationIds);
}
