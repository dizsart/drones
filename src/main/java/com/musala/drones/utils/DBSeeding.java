package com.musala.drones.utils;

import com.musala.drones.entities.Drone;
import com.musala.drones.entities.Medication;
import com.musala.drones.service.DroneService;
import com.musala.drones.service.MedicationService;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Service
public class DBSeeding {


    private final DroneService droneService;
    private final MedicationService medicationService;

    @Autowired
    public DBSeeding(DroneService droneService, MedicationService medicationService) {
        this.droneService = droneService;
        this.medicationService = medicationService;
    }

    @PostConstruct
    public void seedDB() {
        String DRONE_FILE = "csvFiles/dummy_drone.csv";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(DRONE_FILE))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                droneService.registerDrone(new Drone(line));
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String MEDICATION_FILE = "csvFiles/dummy_medication.csv";
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(MEDICATION_FILE))){
            String line;
            while((line=bufferedReader.readLine())!=null){
                medicationService.addMedication(new Medication(line));
            }
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
