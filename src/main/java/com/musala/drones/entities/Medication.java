package com.musala.drones.entities;

import com.musala.drones.DTOs.MedicationDto;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private Double weightInGrams;
    @Column(nullable = false)
    private String code;//check code string
    private String imageUrl;
    private boolean loadedOnDrone = false;

    public Medication(MedicationDto medicationDto) {
        this.weightInGrams = medicationDto.getWeight();
        this.code = medicationDto.getCode();
        this.imageUrl = medicationDto.getImageUrl();
    }

    public Medication(String dummyDataLine){
        String[] rowData = dummyDataLine.split(",");
        this.weightInGrams = Double.valueOf(rowData[0]);
        this.code = rowData[1];
        this.imageUrl = rowData[2];
        this.loadedOnDrone = Boolean.parseBoolean(rowData[3]);
    }
}
