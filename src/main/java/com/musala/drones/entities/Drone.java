package com.musala.drones.entities;

import com.musala.drones.DTOs.DroneDto;
import com.musala.drones.enums.Model;
import com.musala.drones.enums.State;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String serialNumber;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Model model;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private State droneState = State.IDLE;
    private Double loadWeightGrams;
    @Column(nullable = false)
    private Double batteryPercentage;
    @Column(nullable = false)
    private Boolean isAvailable;
    @OneToMany
    private List<Medication> medications;

    public Drone(DroneDto droneDto) {
        this.serialNumber = droneDto.getSerialNumber();
        this.model = droneDto.getModel();
        this.loadWeightGrams = droneDto.getWeightLimit();
        this.batteryPercentage = droneDto.getBatteryPercentage()/100;
    }

    public Drone(String dummyDataLine) {
        String[] rowData = dummyDataLine.split(",");
        this.serialNumber = rowData[0];
        this.model = Model.valueOf(rowData[1].trim());
        this.droneState = State.valueOf(rowData[2].trim());
        this.loadWeightGrams = Double.valueOf(rowData[3]);
        this.batteryPercentage = Double.parseDouble(rowData[4])/100;
        this.isAvailable = Boolean.valueOf(rowData[5].trim());
    }
}
