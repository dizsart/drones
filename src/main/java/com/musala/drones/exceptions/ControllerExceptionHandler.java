package com.musala.drones.exceptions;

import org.springdoc.api.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;


@ControllerAdvice
@ResponseBody
public class ControllerExceptionHandler extends RuntimeException{
    @ExceptionHandler(DroneNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage droneNotFoundException(DroneNotFoundException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage("DRONE NOT FOUND, WITH ID: "+ex.getMessage());
        return message;
    }

    @ExceptionHandler(DroneWeightExceededException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage droneWeightExceededException(DroneNotFoundException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage("DRONE DID NOT LOAD SUCCESSFULLY.PLEASE CHECK WEIGHT CAPACITY");
        return message;
    }
    @ExceptionHandler(BatteryLowException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage batteryLowException(BatteryLowException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage("DRONE DID NOT LOAD SUCCESSFULLY.PLEASE CHECK BATTERY LEVEL");
        return message;
    }

    @ExceptionHandler(MedicationLoadStatusException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage medicationStatusException(MedicationLoadStatusException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage("MEDICATIONS CANNOT BE NULL. PLEASE CHECK LOAD STATUS OF MEDICATION!");
        return message;
    }

    @ExceptionHandler(InvalidCodeException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage medicationStatusException(InvalidCodeException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage("MEDICATION CODE MUST BE A VALID CODE!");
        return message;
    }

    @ExceptionHandler(UnAvailableDroneException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage noAvailableDrone(UnAvailableDroneException ex, WebRequest request) {
        ErrorMessage message = new ErrorMessage("DRONE IS UNAVAILABLE FOR LOADING!");
        return message;
    }




}
