package com.musala.drones.exceptions;

public class DroneNotFoundException extends Exception{
    public DroneNotFoundException(String message) {
        super(message);
    }
}
