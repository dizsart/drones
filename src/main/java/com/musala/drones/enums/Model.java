package com.musala.drones.enums;

public enum Model {
    LIGHT_WEIGHT, MIDDLE_WEIGHT, CRUISER_WEIGHT
}
