package com.musala.drones.DTOs;

import jakarta.persistence.Column;
import lombok.Data;

@Data
public class MedicationDto {
    private Double weight;
    private String code;//check code string
    private String imageUrl;
}
