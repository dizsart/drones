package com.musala.drones.DTOs;

import lombok.Data;

import java.util.List;
@Data
public class LoadDroneRequest {
    private Long id;
    private List<Long> listOfMedicationsIds;
}
