package com.musala.drones.DTOs;

import com.musala.drones.enums.Model;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;

import java.util.List;

@Data
public class DroneDto {

    private String serialNumber;
    private Model model;
    private Double weightLimit;
    private Double batteryPercentage;
    private Boolean isAvailable = true;

}
