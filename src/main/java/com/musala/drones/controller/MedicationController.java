package com.musala.drones.controller;

import com.musala.drones.DTOs.DroneDto;
import com.musala.drones.DTOs.MedicationDto;
import com.musala.drones.entities.Drone;
import com.musala.drones.entities.Medication;
import com.musala.drones.exceptions.InvalidCodeException;
import com.musala.drones.service.MedicationService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.regex.Pattern;

@RestController("/api/v1/medication")
public class MedicationController {
    private final MedicationService medicationService;
    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @Operation(summary = "Add a medication", description = "Adds a medication using the MedicationDTO object")
    @PostMapping("/add-medication")
    public ResponseEntity<Medication> addDrone(@RequestBody MedicationDto medicationDto) throws InvalidCodeException {
        Boolean isValidCode = Pattern.matches("^[A-Za-z0-9_-]*$", medicationDto.getCode());
        if (!isValidCode){
            throw new InvalidCodeException();
        }
        Medication medication = new Medication(medicationDto);
        return new ResponseEntity<>(medicationService.addMedication(medication), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all medication data", description = "Retrieves all medication data stored")
    @GetMapping("/medications")
    public ResponseEntity<List<Medication>> addDrone(){
        return new ResponseEntity<>(medicationService.findAllMedications(), HttpStatus.OK);
    }
}
