package com.musala.drones.controller;

import com.musala.drones.DTOs.DroneDto;
import com.musala.drones.DTOs.LoadDroneRequest;
import com.musala.drones.entities.Drone;
import com.musala.drones.exceptions.*;
import com.musala.drones.service.DroneService;
import com.musala.drones.service.MedicationService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/api/v1/drone")
public class DroneController {
    private final DroneService droneService;
    private final MedicationService medicationService;
    @Autowired
    public DroneController(DroneService droneService, MedicationService medicationService) {
        this.droneService = droneService;
        this.medicationService = medicationService;
    }

    @Operation(summary = "Adds a drone", description = "Adds a drone using the DroneDto object")
    @PostMapping("/add-drone")
    public ResponseEntity<Drone> addDrone(@RequestBody DroneDto droneDto){
        Drone drone = new Drone(droneDto);
        return new ResponseEntity<>(droneService.registerDrone(drone), HttpStatus.CREATED);
    }

    @Operation(summary = "Loads medication(s) on a drone", description = "Loads medication(s) on a drone using " +
            "LoadDroneRequest(requires Drone ID and a List of Medication ID to be loaded")
    @PostMapping("/load-drone")
    public ResponseEntity<Drone> loadDrone(@RequestBody LoadDroneRequest droneRequest) throws DroneNotFoundException, DroneWeightExceededException, BatteryLowException, MedicationLoadStatusException, UnAvailableDroneException {
        return new ResponseEntity<>(droneService.loadDroneMedication(droneRequest.getId(), medicationService.findListOfMedicationsByIds(droneRequest.getListOfMedicationsIds())), HttpStatus.CREATED);
    }

    @Operation(summary = "Find drone by ID", description = "Retrieves a drone by ID")
    @GetMapping("/drone/{id}")
    public ResponseEntity<Drone> findDrone(@PathVariable(name = "id") Long id) throws DroneNotFoundException {
        return new ResponseEntity<>(droneService.findDroneById(id), HttpStatus.OK);
    }

    @Operation(summary = "Finds all drones", description = "Retrieves all drone information")
    @GetMapping("/drones")
    public ResponseEntity<List<Drone>> findDrone() {
        return new ResponseEntity<>(droneService.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Get drone current load weight", description = "Retrieves the weight of a drone's load by drone ID")
    @GetMapping("/drone-capacity/{id}")
    public ResponseEntity<Double> checkDroneCapacity(@PathVariable(name = "id") Long id) throws DroneNotFoundException {
        Double capacityInfo = droneService.checkDroneLoadCapacity(droneService.findDroneById(id));
        return new ResponseEntity<>(capacityInfo, HttpStatus.OK);
    }


    @Operation(summary = "Get drone battery level", description = "Retrieve current battery level of a drone")
    @GetMapping("/drone-battery/{id}")
    public ResponseEntity<String> checkBatteryLevel(@PathVariable(name = "id") Long id) throws DroneNotFoundException {
        return new ResponseEntity<>(droneService.droneBatteryLevel(id), HttpStatus.OK);
    }

    @Operation(summary = "Get available drone", description = "Retrieves drones available for loading and with battery percentage above 25%")
    @GetMapping("/available-drones")
    public ResponseEntity<List<Drone>> findAvailableDrones(){
        return new ResponseEntity<>(droneService.findAvailableDronesForLoading(), HttpStatus.OK);
    }

}
