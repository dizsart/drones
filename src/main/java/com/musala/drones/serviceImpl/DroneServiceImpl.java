package com.musala.drones.serviceImpl;

import com.musala.drones.entities.Drone;
import com.musala.drones.entities.Medication;
import com.musala.drones.enums.State;
import com.musala.drones.exceptions.*;
import com.musala.drones.repository.DroneRepository;
import com.musala.drones.service.DroneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.text.DecimalFormat;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DroneServiceImpl implements DroneService {
    private final DroneRepository droneRepository;
    private final static double PERCENTAGE_LIMIT = 0.25;
    private static final Logger logger = LoggerFactory.getLogger(DroneServiceImpl.class);


    @Autowired
    public DroneServiceImpl(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @Override
    public Drone registerDrone(Drone drone) {
        return droneRepository.save(drone);
    }

    @Override
    public Drone loadDroneMedication(Long droneId, List<Medication> medications) throws DroneNotFoundException, DroneWeightExceededException, BatteryLowException, MedicationLoadStatusException, UnAvailableDroneException {
        if (medications.isEmpty()){
            throw new MedicationLoadStatusException();
        }
        Drone drone = droneRepository.findById(droneId).orElseThrow(()->new DroneNotFoundException(String.valueOf(droneId)));
        if (!drone.getIsAvailable()){
            throw new UnAvailableDroneException();
        }
        medications = medications.stream().peek(medication -> medication.setLoadedOnDrone(true)).collect(Collectors.toList());
        drone.setMedications(medications);
        drone.setDroneState(State.LOADING);
        Double weight = checkDroneLoadCapacity(drone);
        if (weight>500.0){
            drone.setMedications(null);
            droneRepository.save(drone);
            throw new DroneWeightExceededException();
        }
        if (drone.getBatteryPercentage()<=0.25){
            drone.setMedications(null);
            drone.setDroneState(State.IDLE);
            droneRepository.save(drone);
            throw new BatteryLowException();
        }
        drone.setIsAvailable(false);
        drone.setLoadWeightGrams(weight);
        drone.setDroneState(State.LOADED);
        drone = droneRepository.save(drone);
        return drone;
    }

    @Override
    public String droneBatteryLevel(Long droneId) throws DroneNotFoundException {
        Drone drone = droneRepository.findById(droneId).orElseThrow(()->new DroneNotFoundException(String.valueOf(droneId)));
        return new DecimalFormat("#").format(drone.getBatteryPercentage()*100)+"%";
    }

    @Override
    public Double checkDroneLoadCapacity(Drone drone) {
        double weightLimit = drone.getMedications().stream().mapToDouble(Medication::getWeightInGrams).sum();
        return weightLimit;
    }

    @Override
    public List<Drone> findAvailableDronesForLoading() {
        List<Drone> drones =  droneRepository.findAllByIsAvailableIsTrueAndBatteryPercentageIsGreaterThan(PERCENTAGE_LIMIT);
        return drones.isEmpty()?null:drones;
    }
    @Override
    public Drone findDroneById(Long droneId) throws DroneNotFoundException {
        return droneRepository.findById(droneId).orElseThrow(()->new DroneNotFoundException(String.valueOf(droneId)));

    }
    @Override
    public Drone changeDroneState(Long droneId, String droneState) throws DroneNotFoundException, BatteryLowException {
        Drone drone = droneRepository.findById(droneId).orElseThrow(()->new DroneNotFoundException(String.valueOf(droneId)));
        if (drone.getBatteryPercentage()<=0.25){
            drone.setMedications(null);
            drone.setDroneState(State.IDLE);
            droneRepository.save(drone);
            throw new BatteryLowException();
        }
        drone.setDroneState(State.valueOf(droneState));
        return droneRepository.save(drone);
    }

    @Override
    @Scheduled(fixedRate = 30000) // Run every hour
    public void checkBatteryLevels() {
            droneRepository.findAll().forEach(drone -> logger.info(String.format("DRONE ID --> %s,  BATTERY LEVEL --> %s PERCENTAGE, TIME --> %s", drone.getId(), drone.getBatteryPercentage()*100, LocalTime.now())));

    }

    @Override
    public List<Drone> findAll() {
        return droneRepository.findAll();
    }
}
