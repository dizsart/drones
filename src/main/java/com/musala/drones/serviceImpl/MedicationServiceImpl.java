package com.musala.drones.serviceImpl;

import com.musala.drones.entities.Medication;
import com.musala.drones.repository.MedicationRepository;
import com.musala.drones.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicationServiceImpl implements MedicationService {
    private final MedicationRepository medicationRepository;
    @Autowired
    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    public Medication addMedication(Medication medication) {
        return medicationRepository.save(medication);
    }

    @Override
    public List<Medication> findAllMedications() {
        return medicationRepository.findAll();
    }

    @Override
    public List<Medication> findListOfMedicationsByIds(List<Long> medicationIds) {
        return medicationRepository.findByLoadedOnDroneFalseAndIdIn(medicationIds);
    }
}
