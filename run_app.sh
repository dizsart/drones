#!/bin/bash

# Build the Spring Boot application with Maven
mvn clean package

# Run the Spring Boot application
java -jar target/Drones-0.0.1-SNAPSHOT.jar